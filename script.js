let signup = document.querySelector("#signup");
let login = document.querySelector("#login");

document.querySelector("#signupClick").addEventListener("click", (e) => {
    e.preventDefault();

    signup.classList.remove("form-hidden");
    login.classList.add("form-hidden");
})

document.querySelector("#loginClick").addEventListener("click", (e) => {
    e.preventDefault();

    signup.classList.add("form-hidden");
    login.classList.remove("form-hidden");
})

signup.addEventListener("submit", (e) => {
    const errors = []
    document.querySelector(".errorSign").innerHTML = "";

    let username = document.querySelector("#username");
    let password = document.querySelector("#password");
    let confirmPassword = document.querySelector("#confirmPassword");
    let email = document.querySelector("#email");
    let phone = document.querySelector("#phone");

    if (typeof username.value != "string" || username.value == "") {
        errors.push("Entered username is not valid");
    }
    console.log(password.value.length < 8);
    if (password.value.length < 8) {
        errors.push("Length of password should be atleast 8");
    }
    console.log(password.value, confirmPassword.value);
    if (password.value != confirmPassword.value) {
        errors.push("Passwords don't match");
    }
    if (phone.value.length !== 10) {
        errors.push("Phone number should be of 10 digit");
    }

    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (!re.test(email.value)) {
        errors.push("Enter a valid email address");
    }

    errors.forEach(element => {
        const p = document.createElement("p")
        p.append(element)

        document.querySelector(".errorSign").append(p)
    })

    if (errors.length == 0) {
        signup.classList.add("form-hidden");
        login.classList.remove("form-hidden");
    }

    e.preventDefault();
})

login.addEventListener('submit', (e) => {
    const errors = []
    document.querySelector(".error").innerHTML = "";
    let username = document.querySelector("#userId");
    let password = document.querySelector("#password");

    if (typeof username.value != "string" || username.value == "") {
        errors.push("Entered username is not valid");
    }
    if (password.value.length < 8) {
        errors.push("Length of password should be atleast 8");
    }

    errors.forEach(element => {
        const p = document.createElement("p")
        p.append(element)

        document.querySelector(".error").append(p)
    })

    e.preventDefault();
    if (errors.length == 0) {
        window.location.assign("/welcome.html");
    }
})